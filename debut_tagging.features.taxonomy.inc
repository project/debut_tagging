<?php

/**
 * Implementation of hook_taxonomy_default_vocabularies().
 */
function debut_tagging_taxonomy_default_vocabularies() {
  // Allow other modules to register their content types for tagging.
  $data = module_invoke_all('debut_node_type_data');
  $node_types = array();
  foreach ($data as $type => $options) {
    if (isset($options['tagging']) && $options['tagging'] == TRUE) {
      $node_types[$type] = $type;
    }
  }
  return array(
    'tags' => array(
      'name' => 'Tags',
      'description' => '',
      'help' => '',
      'relations' => '1',
      'hierarchy' => '0',
      'multiple' => '0',
      'required' => '0',
      'tags' => '1',
      'module' => 'features_tags',
      'weight' => '0',
      'nodes' => $node_types,
    ),
  );
}
