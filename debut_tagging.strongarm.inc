<?php

/**
 * Implementation of hook_strongarm().
 */
function debut_tagging_strongarm() {
  $export = array();
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'extractor_simple_vocabularies';
  $strongarm->value = array(
    'features_tags' => TRUE,
  );

  $export['extractor_simple_vocabularies'] = $strongarm;

  // Fetch the correct vocabulary ID.
  if ($vid = db_result(db_query("SELECT vid FROM {vocabulary} WHERE module = 'features_tags'"))) {
    $strongarm = new stdClass;
    $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
    $strongarm->api_version = 1;
    $strongarm->name = 'tagging_inject_' . $vid;
    $strongarm->value = 1;

    $export['tagging_inject_' . $vid] = $strongarm;
  }
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'tagging_show_suggestion_example';
  $strongarm->value = 0;

  $export['tagging_show_suggestion_example'] = $strongarm;
  return $export;
}
